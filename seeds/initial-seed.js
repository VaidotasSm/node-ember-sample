exports.seed = function (knex, Promise) {
    return knex('user')
        .del()
        .then(() => knex('user').insert([
            {username: 'admin', password: 'pass'},
            {username: 'user1', password: 'pass'},
        ]))
        .then(() => knex('blog_entry').insert([
            {title: 'Morning News', body: `Today's morning news are full of interesting events.`},
            {title: 'Breaking News', body: `We have some breaking news, wait for it...`}
        ]))
        .then(() => console.log('Seed done!'));
};
