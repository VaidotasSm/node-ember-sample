const gulp = require('gulp');
const mocha = require('gulp-mocha');
const nodemon = require('gulp-nodemon');
const eslint = require('gulp-eslint');

gulp.task('test', function () {
    return gulp
        .src('api/src/**/*.test.js', {read: false})
        .pipe(mocha({reporter: 'dot'}))
});

gulp.task('lint', function () {
    return gulp
        .src(['api/src/**/*.js', 'app/**/*.js', 'tests/**/*.js'])
        .pipe(eslint({configFile: './eslintrc.json'}))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('start-dev', function () {
    nodemon({
        script: 'api/src/main.js',
        args: ['dev', '--list'],
        ext: 'js html',
        ignore: [
        	'./node_modules', './seeds', './migrations',
			'./app', './bower_components', './config', './tmp', './tests',
			'*.test.js'
		],
        env: {
            PORT: 8080,
            PROFILE: 'dev'
        }
    }).on('restart', () => {
        console.log('Restarting…');
    });
});
