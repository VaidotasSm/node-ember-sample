exports.up = function (knex, Promise) {
    return knex.schema
        .createTable('blog_entry', tbl => {
            tbl.increments('id');
            tbl.string('title', 50).notNullable();
            tbl.string('body', 1000).notNullable();
            tbl.date('created').notNullable().defaultTo(knex.fn.now());
        })
        .createTable('user', tbl => {
            tbl.increments('id');
            tbl.string('username', 30).notNullable();
            tbl.string('password', 100).notNullable();
        });
};

exports.down = function (knex, Promise) {
    return knex.schema
        .dropTable('blog_entry');
};
