import Ember from 'ember';
import {HttpUtils} from '../../helpers/http';

export default Ember.Route.extend({
	mainstore: Ember.inject.service('mainstore'),
	toast: Ember.inject.service(),
	beforeModel() {
		if (!this.get('mainstore').get('token')) {
			this.transitionTo('login');
		}
	},
	model() {
		return Ember.$.getJSON('http://localhost:8080/api/blog/entries')
			.fail((res) => {
				HttpUtils.handleError(this.toast, res);
			});
	}
});
