import Ember from 'ember';
import {HttpUtils} from '../../helpers/http';

export default Ember.Route.extend({
	mainstore: Ember.inject.service('mainstore'),
	beforeModel() {
		if (!this.get('mainstore').get('token')) {
			this.transitionTo('login');
		}
	},
	model({entryId}) {
		const token = this.get('mainstore').get('token');
		Ember.$.ajaxSetup({
			headers : {
				'Authorization' : `Bearer ${token}`
			}
		});
		return Ember.$.getJSON(`http://localhost:8080/api/blog/entries/${entryId}`)
			.fail((res) => {
				HttpUtils.handleError(this.toast, res);
			});
	}
});
