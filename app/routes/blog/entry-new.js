import Ember from 'ember';
import {HttpUtils} from '../../helpers/http';

export default Ember.Route.extend({
	mainstore: Ember.inject.service('mainstore'),
	beforeModel() {
		if (!this.get('mainstore').get('token')) {
			this.transitionTo('login');
		}
	},
	model() {
		return {
			title: '',
			body: ''
		};
	},
	actions: {
		createEntry() {
			const token = this.get('mainstore').get('token');
			Ember.$.ajaxSetup({
				headers : {
					'Authorization' : `Bearer ${token}`
				}
			});

			let data = {
				title: this.controller.get('model.title'),
				body: this.controller.get('model.body')
			};
			Ember.$.ajax({
				type: 'POST',
				url: 'http://localhost:8080/api/blog/entries',
				data: data
			})
				.done(() => {
					this.toast.success('Success', 'Created');
					this.transitionTo('blog');
				})
				.fail((res) => {
					HttpUtils.handleError(this.toast, res);
				});
		}
	}
});
