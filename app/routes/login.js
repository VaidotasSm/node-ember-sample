import Ember from 'ember';
import {HttpUtils} from '../helpers/http';

export default Ember.Route.extend({
	mainstore: Ember.inject.service('mainstore'),
	toast: Ember.inject.service(),
	model() {
		return {
			username: '',
			password: ''
		};
	},
	actions: {
		login() {
			let data = {
				username: this.controller.get('model.username'),
				password: this.controller.get('model.password')
			};
			Ember.$.ajax({
				type: 'POST',
				url: 'http://localhost:8080/api/auth/login',
				data: data
			})
				.done((res) => {
					this.get('mainstore').set('token', res.access_token);
					this.transitionTo('blog');
				})
				.fail((res) => {
					HttpUtils.handleError(this.toast, res);
				});
		}
	}
});
