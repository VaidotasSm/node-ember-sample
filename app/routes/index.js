import Ember from 'ember';

export default Ember.Route.extend({
	mainstore: Ember.inject.service('mainstore'),
	beforeModel() {
		if (!this.get('mainstore').get('token')) {
			this.transitionTo('login');
		} else {
			this.transitionTo('blog');
		}
	}
});
