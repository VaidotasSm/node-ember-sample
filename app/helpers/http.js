const HttpUtils = {
	/**
	 * Quick and Hacky
	 */
	handleError(toast, errResponse) {
		let message = errResponse.responseJSON && errResponse.responseJSON.message ?
			errResponse.responseJSON.message :
			'Error while making request';
		let field = '';
		if (errResponse.status === 400) {
			field = errResponse.responseJSON && errResponse.responseJSON.field ? errResponse.responseJSON.field : null;
		}
		toast.error(`${field} ${message}`, `Error (${errResponse.status})`);
	}
};

export {HttpUtils};
