# Sample Node-Ember application

## Technical Details

### Prerequisites
* Node 6.9+

### Usage

Start:

1. `npm install`
2. `npm run build` - build Ember UI.
3. `npm start` - start application (DEV mode).
4. Open [http://localhost:8080/blog-ui](http://localhost:8080/blog-ui). Login with `admin/pass`.

Other commands:

* `npm test`, `npm run lint` - testing and linting.
* `npm run start:ui`, `npm run test:ui` - start Ember dev server, run Ember tests.


## Known Issues
* SQLite in-memory DB often stops working correctly after some time. Need to `npm start` again.