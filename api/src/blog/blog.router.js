const express = require('express');

class BlogRouter {
	constructor(blogRepo) {
		if (!blogRepo) {
			throw 'blogRepo must be defined';
		}
		this.repo = blogRepo;
	}

	getAll(req, res, next) {
		this.repo.findBlogEntries()
			.then((rows) => {
				res.status(200).json({
					entries: rows
				});
			})
			.catch((err) => next(err));
	}

	getOne(req, res, next) {
		if (!req.userDetails) {
			res.sendStatus(401);
			return;
		}

		const id = parseInt(req.params.id);
		if (!id) {
			res.status(400).json({message: `Could not identify entry by '${req.params.id}'`});
			return;
		}

		this.repo.findBlogEntries({id})
			.then((rows) => {
				if (rows.length !== 1) {
					res.sendStatus(404);
					return;
				}
				res.status(200).json(rows[0]);
			})
			.catch((err) => next(err));
	}

	post(req, res, next) {
		if (!req.userDetails) {
			res.sendStatus(401);
			return;
		}

		const entry = req.body;
		const validationErr = this.validateRequest(entry);
		if (validationErr) {
			res.status(400).json(validationErr);
			return;
		}

		entry.id = null;
		this.repo.findBlogEntries({title: entry.title}).then((rows) => {
			if (rows.length > 0) {
				res.status(409).json({field: 'title', message: 'Such entry already exist'});
				return;
			}

			this.repo.insertBlogEntry(entry)
				.then((id) => {
					res.setHeader('Location', `${req.protocol}://${req.headers.host}${req.originalUrl}/${id}`);
					res.status(201).json(Object.assign({}, entry, {id}));
				})
				.catch((err) => {
					next(err);
				});
		});
	}

	put(req, res, next) {
		if (!req.userDetails) {
			res.sendStatus(401);
			return;
		}

		const id = parseInt(req.params.id);
		if (!id) {
			res.status(400).json({message: 'Could not identify entry'});
			return;
		}
		const entry = req.body;
		const validationErr = this.validateRequest(entry);
		if (validationErr) {
			res.status(400).json(validationErr);
			return;
		}

		entry.id = id;
		this.repo.updateBlogEntry(id, entry)
			.then((rowsAffected) => {
				if (rowsAffected === 0) {
					res.status(404).json({message: 'No such entry'});
					return;
				}
				res.sendStatus(200);
			})
			.catch((err) => {
				next(err);
			});
	}

	validateRequest(entryBody) {
		if (!entryBody || Object.keys(entryBody).length === 0) {
			return {message: 'Request body cannot be empty'};
		}
		if (!entryBody.title) {
			return {field: 'title', message: 'Title cannot be empty'};
		}
		if (!entryBody.body) {
			return {field: 'body', message: 'Body cannot be empty'};
		}
	}

	initRouter() {
		return express.Router()
			.get('/entries', (req, res, next) => this.getAll(req, res, next))
			.get('/entries/:id', (req, res, next) => this.getOne(req, res, next))
			.post('/entries', (req, res, next) => this.post(req, res, next))
			.put('/entries/:id', (req, res, next) => this.put(req, res, next));
	}
}

module.exports = BlogRouter;
