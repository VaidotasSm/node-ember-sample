class BlogRepository {
	constructor(knex) {
		if (!knex) {
			throw 'knex must be defined';
		}
		this.knex = knex;
	}

	findBlogEntries(where) {
		if (!where) {
			return this.knex.select('id', 'title', 'body', 'created').from('blog_entry');
		}
		return this.knex.select('id', 'title', 'body', 'created').from('blog_entry').where(where);
	}

	insertBlogEntry(entry) {
		return this.knex.insert(entry).into('blog_entry');
	}

	updateBlogEntry(id, entry) {
		return this.knex('blog_entry').where({id: id}).update(entry);
	}
}

module.exports = BlogRepository;
