const knexConfig = require('./../../knexfile');
const knex = require('knex')(knexConfig.development);
const winston = require('winston');
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const JwtService = require('./auth/jwt.service.js');
const AuthRouter = require('./auth/auth.router.js');
const UserRepository = require('./user/user.repository.js');
const BlogRouter = require('./blog/blog.router.js');
const BlogRepository = require('./blog/blog.repository.js');

const profiles = {
	PROFILE_DEV: 'dev',
	PROFILE_PROD: 'prod'
};

const rootRouter = () => {
	const jwt = new JwtService();
	const auth = new AuthRouter(new UserRepository(knex), jwt);
	const blog = new BlogRouter(new BlogRepository(knex));
	return express.Router()
		.use((req, res, next) => {
			res.header('Access-Control-Allow-Origin', '*');
			res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
			res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
			let requestInfo = `${req.originalUrl} - ${req.method}`;
			jwt.authenticateUser(req)
				.then((user) => {
					winston.info(`${requestInfo} (Authenticated)`);
					req.userDetails = user;
				})
				.catch(() => {
					winston.info(`${requestInfo} (Unauthenticated)`);
				})
				.then(() => {
					next();
				});
		})
		.use('/api/auth', auth.initRouter())
		.use('/api/blog', blog.initRouter())
		.get('*', (req, res) => {
			winston.warn(`404 - Not Found`);
			res.status(404).json({message: 'Page not Found'});
		});
};

class AppServer {
	constructor(profile = profiles.PROFILE_DEV, port = 8080) {
		this.profile = profile;
		this.port = port;
		this.app = null;
		this.handle = null;
	}

	start() {
		const initApp = () => {
			this.app = express();
			this.app.use(bodyParser.json());
			this.app.use(bodyParser.urlencoded({extended: true}));
			this.app.use('/blog-ui', express.static(path.join(__dirname, '../..', 'dist')));

			this.app.use(rootRouter());
			this.app.use((err, req, res, next) => {
				if (!err) {
					return next();
				}
				winston.error(err);
				res.status(500).json({
					message: 'Something went wrong'
				});
			});

			this.handle = this.app.listen(this.port, () => {
				winston.info(`Started on port ${this.port},profile '${this.profile}'`);
			});
			return this.handle;
		};


		if (this.profile === profiles.PROFILE_DEV) {
			winston.info('Running migrations');
			return knex.migrate.latest()
				.then(() => knex.seed.run())
				.catch((err) => winston.error(err))
				.then(() => {
					winston.info('Migrations done');
					return Promise.resolve(initApp());
				});
		}
		return Promise.resolve(initApp());
	}
}

module.exports = {
	AppServer: AppServer,
	profiles: profiles
};
