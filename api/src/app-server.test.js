const expect = require('chai').expect;
const supertest = require('supertest-as-promised');

const appServer = require('./app-server');
const AppServer = appServer.AppServer;
const profiles = appServer.profiles;

let agent;
const server = new AppServer(profiles.PROFILE_DEV, 3000);
before((done) => {
	server.start().then(() => {
		agent = supertest.agent(server.app);
		done();
	});
});
after((done) => {
	server.handle.close();
	done();
});

describe('User Authentication', () => {
	it('Can authenticate with default user', (done) => {
		agent.post('/api/auth/login').send({username: 'admin', password: 'pass'})
			.then((res) => {
				expect(res.status).to.equal(200);
				expect(res.body.access_token).to.not.be.undefined;
			})
			.then(() => done())
			.catch((err) => done(err));
	});
});

describe('Blog API', () => {
	const authenticateMust = () => {
		return agent.post('/api/auth/login').send({username: 'admin', password: 'pass'})
			.then((res) => {
				expect(res.status).to.equal(200);
				expect(res.body.access_token).to.not.be.undefined;
				return `Bearer ${res.body.access_token}`;
			});
	};

	it('Endpoints are protected', (done) => {
		agent.get('/api/blog/entries').expect(200)
			.then(() => agent.post('/api/blog/entries').send({}).expect(401))
			.then(() => agent.put('/api/blog/entries/111').send({}).expect(401))
			.then(() => agent.get('/api/blog/entries/111').expect(401))
			.then(() => done())
			.catch((err) => done(err));
	});

	it('Can retrieve existing blog entries', (done) => {
		agent.get('/api/blog/entries')
			.expect(200)
			.then((res) => {
				expect(res.body.entries).to.be.instanceof(Array);
				expect(res.body.entries).to.have.lengthOf(2);
			})
			.then(() => done())
			.catch((err) => done(err));
	});

	it('Creates blog entry', (done) => {
		let token;
		authenticateMust()
			.then((tok) => token = tok)
			.then(() => agent.post('/api/blog/entries')
				.send({title: 'Title01', body: 'Body01'})
				.set('Authorization', token)
				.expect(201)
				.expect('Location', new RegExp('/api/blog/entries/'))
				.then((res) => {
					expect(res.body.id).to.exist;
					return res.body.id;
				})
			)
			.then((id) => agent.get(`/api/blog/entries/${id}`).set('Authorization', token)
				.then((res) => {
					expect(res.status).to.equal(200);
					expect(res.body.title).to.equal('Title01');
					expect(res.body.body).to.equal('Body01');
					expect(res.body.created).to.exist;
				})
			)
			.then(() => done())
			.catch((err) => done(err));
	});

	it('Cannot create blog entry with invalid values', (done) => {
		let token;
		authenticateMust()
			.then((tok) => token = tok)
			.then(() => agent.post('/api/blog/entries').send(null).set('Authorization', token)
				.expect(400)
				.then(() => agent.post('/api/blog/entries')
					.send({title: '', body: 'Body01'}).set('Authorization', token)
					.expect(400)
					.then((res) => {
						expect(res.body.field).to.equal('title');
					})
				)
			)
			.then(() => agent.post('/api/blog/entries')
				.send({title: 'Title01', body: ''}).set('Authorization', token)
				.expect(400)
				.then((res) => {
					expect(res.body.field).to.equal('body');
				})
			)
			.then(() => done())
			.catch((err) => done(err));
	});

	it('Cannot create blog entry with same title', (done) => {
		let token;
		authenticateMust()
			.then((tok) => token = tok)
			.then(() => agent.post('/api/blog/entries')
				.send({title: 'SameTitle', body: 'Body'}).set('Authorization', token)
				.expect(201)
				.then((res) => res.body.id)
			)
			.then(() => agent.post('/api/blog/entries')
				.send({title: 'SameTitle', body: 'Body'}).set('Authorization', token)
				.expect(409)
				.then((res) => {
					expect(res.body.field).to.equal('title');
				})
			)
			.then(() => done())
			.catch((err) => done(err));
	});

	it('Updates blog entry', (done) => {
		let token;
		let entryId;
		authenticateMust()
			.then((tok) => token = tok)
			.then(() => agent.post('/api/blog/entries')
				.send({title: 'Title02', body: 'Body02'}).set('Authorization', token)
				.expect(201)
				.then((res) => entryId = res.body.id)
			)
			.then(() => agent.put(`/api/blog/entries/${entryId}`)
				.send({title: 'Title02e1', body: 'Body02e1'}).set('Authorization', token)
				.expect(200)
			)
			.then(() => agent.get(`/api/blog/entries/${entryId}`).set('Authorization', token)
				.then((res) => {
					expect(res.status).to.equal(200);
					expect(res.body.title).to.equal('Title02e1');
					expect(res.body.body).to.equal('Body02e1');
					expect(res.body.created).to.exist;
				})
			)
			.then(() => done())
			.catch((err) => done(err));
	});

	it('Cannot update blog entry with invalid values', (done) => {
		let token;
		let entryId;
		authenticateMust()
			.then((tok) => token = tok)
			.then(() => agent.post('/api/blog/entries')
				.send({title: 'Title03', body: 'Body03'}).set('Authorization', token)
				.expect(201)
				.then((res) => {
					entryId = res.body.id;
				})
			)
			.then(() => agent.put(`/api/blog/entries/${entryId}`).set('Authorization', token)
				.expect(400)
				.then((res) => {
					expect(res.body.field).to.not.exist;
				})
			)
			.then(() => agent.put(`/api/blog/entries/${entryId}`)
				.send({title: '', body: 'Body01'}).set('Authorization', token)
				.expect(400)
				.then((res) => {
					expect(res.body.field).to.equal('title');
				})
			)
			.then(() => agent.put(`/api/blog/entries/${entryId}`)
				.send({title: 'Title01', body: ''}).set('Authorization', token)
				.expect(400)
				.then((res) => {
					expect(res.body.field).to.equal('body');
				})
			)
			.then(() => done())
			.catch((err) => done(err));
	});
});

