const jwt = require('jwt-simple');

const JWT_ALGORITHM = 'HS512';
const HEADER_PREFIX = 'Bearer ';

class JwtService {
	constructor(secretKey = 'secret123') {
		this.secretKey = secretKey;
	}

	encode(userDetails) {
		return jwt.encode(userDetails, this.secretKey, JWT_ALGORITHM);
	}

	decode(token) {
		if (!token) {
			return null;
		}
		try {
			return Promise.resolve(jwt.decode(token, this.secretKey, false, JWT_ALGORITHM));
		} catch (err) {
			return Promise.reject(err);
		}
	}

	authenticateUser(req) {
		let authHeader = req.headers.authorization;
		if (!authHeader || authHeader.indexOf(HEADER_PREFIX) === -1) {
			return Promise.reject('No Authorization header provided');
		}
		return this.decode(authHeader.replace(HEADER_PREFIX, ''));
	}
}

module.exports = JwtService;
