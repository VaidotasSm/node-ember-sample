const express = require('express');

class AuthRouter {

	constructor(userRepo, jwtService) {
		if (!userRepo) {
			throw 'userRepo must be defined';
		}
		this.userRepo = userRepo;

		if (!jwtService) {
			throw 'jwtService must be defined';
		}
		this.jwtService = jwtService;
	}

	authenticate(req, res, next) {
		if (!req.body) {
			res.status(400).json({message: 'Body cannot be empty'});
			return;
		}
		const credentials = req.body;
		if (!credentials.username) {
			res.status(400).json({field: 'username', message: 'Cannot be empty'});
			return;
		}
		if (!credentials.password) {
			res.status(400).json({field: 'password', message: 'Cannot be empty'});
			return;
		}
		this.userRepo.findUserByName(credentials.username)
			.then((user) => {
				if (!user) {
					res.status(401).json({message: 'User does not exist'});
					return;
				}
				if (credentials.password !== user.password) {
					res.status(401).json({message: 'Incorrect credentials'});
					return;
				}
				res.status(200).json({access_token: this.jwtService.encode(user)}); // eslint-disable-line camelcase
			})
			.catch((err) => next(err));
	}

	initRouter() {
		return express.Router()
			.post('/login', (req, res, next) => this.authenticate(req, res, next));
	}
}

module.exports = AuthRouter;
