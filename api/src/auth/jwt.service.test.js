const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;

const JwtService = require('./jwt.service.js');

describe('JWT', () => {

	it('Encoded token is decoded', (done) => {
		const jwt = new JwtService('secret123');
		const token = jwt.encode({username: 'user1'});
		expect(jwt.decode(token)).to.eventually.deep.equal({username: 'user1'}).notify(done);
	});

	it('Cannot decode with wrong secret', (done) => {
		const jwt1 = new JwtService('secretGood');
		const jwt2 = new JwtService('secretBad');

		const token = jwt1.encode({username: 'user1'});
		expect(jwt2.decode(token)).to.be.rejected.notify(done);
	});

	it('Verifies request', (done) => {
		const jwt = new JwtService('secretGood');
		const token = jwt.encode({username: 'user1'});

		const req = {headers: {authorization: `Bearer ${token}`}};
		expect(jwt.authenticateUser(req)).to.eventually.deep.equal({username: 'user1'}).notify(done);
	});

	it('Fails to verify request when wrong token', (done) => {
		const jwt1 = new JwtService('secretGood');
		const jwt2 = new JwtService('secretBad');
		const token = jwt1.encode({username: 'user1'});

		const req = {headers: {authorization: `Bearer ${token}`}};
		expect(jwt2.authenticateUser(req)).to.be.rejected.notify(done);
	});

});
