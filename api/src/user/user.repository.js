class UserRepository {
	constructor(knex) {
		if (!knex) {
			throw 'knex must be defined';
		}
		this.knex = knex;
	}

	findUserByName(username) {
		return this.knex.select('username', 'password').from('user').where({username})
			.then((users) => {
				if (users.length > 1) {
					return Promise.reject('Expected one user but found many');
				}
				if (users.length === 0) {
					return null;
				}
				return users[0];
			});
	}
}

module.exports = UserRepository;
